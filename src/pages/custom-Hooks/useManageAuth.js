function useManageAuth () {
  const storeProjectId = (id) => {
    if (id) {
      const getProjectData = { projectID: id.projectID, projectName: id.projectName }
      localStorage.setItem('Node_Project', JSON.stringify(getProjectData))
      const getLoacl = localStorage.getItem('Node_Project')
    } else {
      const getloacl = localStorage.getItem('Node_Project')
      const data = JSON.parse(getloacl)
      return data
    }
  }

  const logOut = () => {
    localStorage.removeItem('auth-token')
    // navigate(`/`);
  }

  return {
    storeProjectId,
    logOut
  }
}

export default useManageAuth
