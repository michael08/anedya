import React from 'react'
import { BaseURL } from '../../App'

function useManageApi () {
  const token = localStorage.getItem('auth-token')
  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: token
    }
  }
  const manageApicalls = async (url, payload) => {
    if (url === '/favourite_project/user/') {
      return await BaseURL.delete(`${url}${payload}`)
    }
    return await BaseURL.post(`${url}`, payload, config)
  }

  return {
    manageApicalls
  }
}

export default useManageApi
