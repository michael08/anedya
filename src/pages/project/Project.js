import React, { useEffect, useState } from "react";
import {
  Box,
  Typography,
  InputAdornment,
  TextField,
  Skeleton,
  TablePagination,
  Tooltip,
  Button,
  Card,
} from "@mui/material";
import Modal from "@mui/material/Modal";
import styled from "@emotion/styled";
import InfoIcon from "@mui/icons-material/Info";
import { useSnackbar } from "notistack";
import { jwtDecode } from "jwt-decode";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import SearchIcon from "@mui/icons-material/Search";
import { tooltipClasses } from "@mui/material/Tooltip";
import ProjectItem from "./ProjectItem";
import CreateProject from "./CreateProject";
import {
  getFavLisitRequest,
  getProductListRequest,
  deleteProjectData,
} from "../../redux/actions/projectAction";

const LightTooltip = styled(({ className, ...props }) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: "white",
    color: "rgba(0, 0, 0, 0.87)",
    boxShadow:
      "rgba(60, 64, 67, 0.3) 0px 1px 2px 0px, rgba(60, 64, 67, 0.15) 0px 2px 6px 2px",
    fontSize: 11,
    width: 700,
    height: 130,
  },
}));

function Project() {
  const navigate = useNavigate();
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [page, setPage] = useState(0);
  const [open, setOpen] = useState(false);
  const [openEdit, setOpenEdit] = useState(false);
  const [editData, setEditData] = useState(null);
  const [getProductList, setGetproductList] = useState([]);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const { enqueueSnackbar } = useSnackbar();
  const menuopen = Boolean(anchorEl);
  const [favorite, setFavorite] = useState([]);
  const [userData, setUser] = React.useState(null);
  const [startingPage, setStartingPage] = useState(0);
  const limit = 10;
  const [param, setParam] = useState({ offset: 0, limit: 10 });
  const [filterProject, setFilterProject] = useState([]);
  const [loading, setLoading] = useState(true);
  const [favoriteList, setFavoriteList] = useState([]);

  useEffect(() => {
    const offset = startingPage * limit;
    const data = {
      offset,
      limit,
    };
    setParam(data);
    dispatch(getProductListRequest(data));
  }, [startingPage, limit]);

  const handlemenuClose = () => {
    setAnchorEl(null);
  };
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value));
    setPage(0);
  };
  const handleOpen = () => setOpen(true);
  const handleClose = () => {
    setOpen(false);
    dispatch(getProductListRequest(param));
  };

  /// dispatch the action
  const dispatch = useDispatch();
  /// get the data from store
  const getProjectData = useSelector((state) => state?.project);
  // destructure the store
  const { favLisit, getProduct, deleteResponse } = getProjectData;

  const {projects ,count} =getProduct
  /// useEffect to manage delete call

  useEffect(() => {
    setFavoriteList(Array.isArray(favLisit) ? favLisit : []);
  }, [favLisit]);
  useEffect(() => {
    if (deleteResponse.length !== 0) {
      if (deleteResponse.success === true) {
        dispatch(getProductListRequest());
        dispatch(getFavLisitRequest());
        enqueueSnackbar("Project Deleted Successfully", {
          variant: "success",
          autoHideDuration: 2000,
        });
        deleteResponse.success = false;
      } else if (deleteResponse.name === "AxiosError") {
        enqueueSnackbar("Error Project Deleted", {
          variant: "error",
          autoHideDuration: 2000,
        });
        deleteResponse.name = "Error";
      }
    }
  }, [deleteResponse]);

  // useEffect for productList
  useEffect(() => {
    setLoading(true);
    if (getProduct.success === true) {
      setGetproductList(getProduct.projects);
      setFilterProject(getProduct.projects);
      setLoading(false);
    } else if (getProduct.success === false) {
      enqueueSnackbar(`${getProduct.error}`, { variant: "error" });
      setLoading(false);
      getProduct.success = null;
    }
  }, [getProduct]);

  useEffect(() => {
    const authToken = localStorage.getItem("auth-token");
    if (authToken) {
      dispatch(getFavLisitRequest());
      const decodedToken = jwtDecode(authToken);
      setUser(decodedToken);
    } else {
      setUser(null);
    }
  }, []);

  useEffect(() => {
    dispatch(getProductListRequest(param));
  }, [favorite]);

  const handleOpenEdit = (data) => {
    setOpenEdit(true);
    setEditData(data);
    setOpen(true);
  };
  const handleCloseEdit = () => {
    dispatch(getProductListRequest(param));
    setOpenEdit(false);
    setOpen(false)
  };

  const delete_data = async (param) => {
    const id = { projectID: [param] };
    dispatch(deleteProjectData({ projectID: [id.projectID[0].projectID] }));
  };

  const reDirect = (data) => {
    // getprojectid(data);
    navigate("/dashboards/Project/Projectdetails");
  };

  const SearchProject = (event) => {
    const searchData = event.target.value;
    const searchProject = getProductList?.filter((data) =>
      data.projectName.toLowerCase().includes(searchData.toLowerCase())
    );
    setFilterProject(searchProject);
    setPage(0);
  };
  return (
    <div className="projectPage">
      <div className="projectPage-box">
        {loading ? (
          ProjectSkeleton()
        ) : getProductList.length < 0 ? (
          <div className="projectPage-box-welcome">
            <Typography className="projectPage-box-welcome-title">
              Welcome! It appears you haven't started any projects yet. To
              begin, please create a new project
            </Typography>
            <Button
              variant="contained"
              className="projectPage-box-welcome-button"
              onClick={handleOpen}
            >
              Create Project
            </Button>
          </div>
        ) : (
          <div className="projectPage-viewProject">
            <div className="projectPage-viewProject-box" >
              <Card
                className="projectPage-viewProject-card"
                onClick={handleOpen}
              >
                <Typography className="projectPage-viewProject-card-create">
                  +
                </Typography>
                <Typography className="projectPage-viewProject-card-text">
                  Create Project
                </Typography>
              </Card>

              <div className="projectPage-viewProject-card-favorite-scroll">
                {favoriteList?.map((item, index) => {
                  const fav = projects?.find(
                    (proj) => proj?.projectID === item
                  );
                  if (fav) {
                    item = {
                      ...item,
                      projectName: fav?.projectName,
                      projectID: fav?.projectID,
                    };
                  }
                  return (
                    <Card className="projectPage-viewProject-card-favorite"  style={{ display: 'flex', flexDirection: 'column' }}>
                      <Typography className="projectPage-viewProject-card-text"  style={{ display: 'flex', flexDirection: 'column' }}>
                        {item?.projectName}
                      </Typography>
                    </Card>
                  );
                })}
              </div>
            </div>
          </div>
        )}
      </div>
      <div className="projectList">
        <div className="projectList-box">
          <Typography variant="h6" className="projectList-box-title">
            Project{" "}
            <LightTooltip
              placement="right-start"
              title={
                <div className="projectList-box-title-info">
                  <Typography className="projectList-box-title-info-heading">
                    Information
                  </Typography>
                  <Typography className="project-box-title-info-content">
                    Information for this projectinformation for this project
                  </Typography>
                </div>
              }
            >
              <InfoIcon sx={{ cursor: "pointer" }} id="basic-button" />
            </LightTooltip>
          </Typography>
          <TextField
            InputProps={{
              autoComplete: "off",
              className: "projectList-box-search",
              startAdornment: (
                <InputAdornment position="start">
                  <SearchIcon />
                </InputAdornment>
              ),
            }}
            onChange={SearchProject}
            placeholder="Search"
          />
        </div>
        <div className="projectList-view">
          {filterProject
            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map((user, index) => {
              const project = favoriteList?.find(
                (proj) => proj === user?.projectID
              );
              if (project) {
                user = { ...user, favorite: true };
              } else {
                user = { ...user, favorite: false };
              }
              return (
                <ProjectItem
                  user={user}
                  key={index}
                  delete_data={delete_data}
                  handleOpenEdit={handleOpenEdit}
                />
              );
            })}
          {filterProject.length == 0 && (
            <div className="emptyProjectList">
              <h4>No Data Found</h4>
            </div>
          )}
        </div>
      </div>
      {getProductList?.length > 0 && filterProject.length > 0 && (
        <TablePagination
          component="div"
          count={getProduct.count}
          page={page}
          rowsPerPageOptions={5}
          onPageChange={handleChangePage}
          rowsPerPage={rowsPerPage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      )}
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box className="modal-box">
          {
            !openEdit? <CreateProject moduleTitle="Create" modalClose={handleClose} />
            : <CreateProject moduleTitle="Edit" modalClose={handleCloseEdit}  editData={editData}/>
          }
         
        </Box>
      </Modal>
    </div>
  );
}

export default Project;

function ProjectSkeleton() {
  return (
    <div>
      <div>
        <div
          style={{
            display: "flex",
            columnGap: "50px",
            flexWrap: "nowrap",
            overflow: "hidden",
            padding: "0 0 0 5px",
          }}
        >
          <div
            style={{
              width: "200px",
              height: "300px",
              flexGrow: "0",
              flexShrink: "0",
            }}
          >
            <Skeleton
              animation="wave"
              variant="rectangular"
              style={{ borderRadius: 10 }}
              width={230}
              height={240}
            />
          </div>

          <div className="nonscroll_fav">
            <div style={{ display: "flex", columnGap: "5px" }}>
              <Skeleton
                animation="pulse"
                variant="rectangular"
                style={{ borderRadius: 10 }}
                width={230}
                height={240}
              />
              <Skeleton
                animation="pulse"
                variant="rectangular"
                style={{ borderRadius: 10 }}
                width={230}
                height={240}
              />
              <Skeleton
                animation="pulse"
                variant="rectangular"
                style={{ borderRadius: 10 }}
                width={230}
                height={240}
              />
            </div>
          </div>
        </div>
      </div>

      <div sx={{ display: "flex", flexDirection: "column", rowGap: "10px" }}>
        <Skeleton
          animation="pulse"
          variant="rectangular"
          style={{ borderRadius: 10 }}
          width={950}
          height={50}
        />
        <Skeleton
          animation="pulse"
          variant="rectangular"
          style={{ borderRadius: 10 }}
          width={950}
          height={50}
        />
        <Skeleton
          animation="pulse"
          variant="rectangular"
          style={{ borderRadius: 10 }}
          width={950}
          height={50}
        />
      </div>
    </div>
  );
}
