import React, { useEffect } from 'react'
import { IconButton } from '@mui/material'
import PropTypes from 'prop-types'
import StarIcon from '@mui/icons-material/Star'
import StarBorderIcon from '@mui/icons-material/StarBorder'
import { useSnackbar } from 'notistack'
import { jwtDecode } from 'jwt-decode'
import useManageApi from '../custom-Hooks/useManageApi'
import { getFavLisitRequest } from '../../redux/actions/projectAction'
import { useDispatch } from 'react-redux'

function Bookmark ({
  value, onChange, reLoade, data, sx
}) {
  const [bookmark, setBookmark] = React.useState(value)
  const { enqueueSnackbar } = useSnackbar()
  const [userData, setUser] = React.useState(null)
  const { manageApicalls } = useManageApi()
  const dispatch = useDispatch()

  React.useEffect(() => {
    if (value !== undefined) {
      setBookmark(value)
    } else {
      setBookmark(false)
    }
  }, [bookmark, onChange, value])

  useEffect(() => {
    const authToken = localStorage.getItem('auth-token')
    if (authToken) {
      const decodedToken = jwtDecode(authToken)
      setUser(decodedToken)
    } else {
      setUser(null)
    }
  }, [])

  const Add_favorite = async () => {
    const Favorite_data = {
      projectid: data?.user?.projectID,
      userid: userData?.UUID
    }
    const response = await manageApicalls('favourite_project', Favorite_data).then((res) => {
      enqueueSnackbar(`${data?.user?.projectName} add to Favorite`, { variant: 'success' })
      dispatch(getFavLisitRequest())
    }).catch((error) => {
      enqueueSnackbar(`${error}`, { variant: 'error' })
    })
  }

  const remove_favorite = async (id) => {
    const Favorite_data = {
      projectid: data?.user?.projectID,
      userid: userData?.UUID
    }
    const response = await manageApicalls('/favourite_project/user/', `${Favorite_data.userid}/${Favorite_data.projectid}`).then((res) => {
      enqueueSnackbar(`${data?.user?.projectName} removed from Favorite`, { variant: 'success' })
      dispatch(getFavLisitRequest())
    }).catch((error) => {
      enqueueSnackbar(`${error}`, { variant: 'error' })
    })
  }

  const handleClick = async () => {
    const token = localStorage.getItem('auth-token')
    if (!bookmark) {
      Add_favorite()
    } else {
      remove_favorite(data?.user?.projectID)
    }
    setBookmark(!bookmark)
  }

  return (
    <IconButton onClick={handleClick} sx={sx}>
      {
        bookmark
          ? (
          <StarIcon fontSize="small" color="warning" />
            )
          : (
          <StarBorderIcon fontSize="small" />
            )
      }
    </IconButton>
  )
}
Bookmark.defaultProps = {
  onChange: () => {
  }
}

Bookmark.propTypes = {
  value: PropTypes.bool,
  onChange: PropTypes.func,
  sx: PropTypes.object
}
export default Bookmark
