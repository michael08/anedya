import { ErrorMessage, Form, Formik } from 'formik'
import React, { useEffect, useState } from 'react'
import {
  Button,
  TextField,
  Box,
  Grid,
  Snackbar,
  Alert,
  Typography
} from '@mui/material'
import DeleteIcon from '@mui/icons-material/Delete'
import * as yup from 'yup'
import { useSnackbar } from 'notistack'
import { useDispatch, useSelector } from 'react-redux'
import useManageApi from '../custom-Hooks/useManageApi'
import {
  createProjectRequest,
  editprojectRequest,
  getProjectDetailsRequest
} from '../../redux/actions/projectAction'
const validationSchema = yup.object({
  projectName: yup
    .string('Enter the Company name')
    .trim()
    .test('no-space', 'Field cannot contain only spaces', (value) =>
      /\S/.test(value)
    )
    .max(255, 'You can enter only 255 characters Only')
    .required('Company Name is required'),
  projectDesc: yup
    .string('Enter the Project Description')
    .trim()
    .test('no-space', 'Field cannot contain only spaces', (value) =>
      /\S/.test(value)
    )
    .max(1000, 'You can enter only 1000 characters Only')
    .required('Project Description is required'),
  projectIdentifier: yup
    .string('Enter the Project Identifier')
    .trim()
    .test('no-space', 'Field cannot contain only spaces', (value) =>
      /\S/.test(value)
    )
    .max(36, 'You can enter only 36 characters Only')
})

const intial_form_value = {
  projectName: '',
  projectDesc: '',
  projectIdentifier: ''
}

function CreateProject ({ modalClose, moduleTitle, editData }) {
  const formikRef = React.useRef(null)
  const { enqueueSnackbar } = useSnackbar()
  const [message, setMessage] = useState('')
  const [severity, setSeverity] = useState('success')
  const [open, setOpen] = useState(false)
  const [disableButton, setDisableButton] = useState(true)
  const [Tags, setTags] = useState([])
  const [initialValue, setInitialValue] = useState(intial_form_value)
  const [productDetails, setproductDetails] = useState({})
  const [Errors, setErrors] = useState([])

  const disPatch = useDispatch()
  const getProjectData = useSelector((state) => state.project)
  const { createProject, getProjectDetails, editProject } = getProjectData
  const { manageApicalls } = useManageApi()

  /// useEffect to controll creat Project
  useEffect(() => {
    if (createProject.success === true) {
      modalClose()
      setInitialValue(intial_form_value)
      enqueueSnackbar(
        `${initialValue?.projectName} Project Created Successfully`,
        { variant: 'Success', autoHideDuration: 2000 }
      )
      createProject.success = ''
    } else if (createProject.response == false) {
      enqueueSnackbar(createProject.error || 'An error occurred while creating the project', {
        variant: 'error',
        autoHideDuration: 2000
      })
      createProject.response = 'Error'
    }
  }, [createProject])

  /// get project details
  useEffect(() => {
    if (getProjectDetails.success === true) {
      const getResponse = getProjectDetails?.projects[0]
      setInitialValue(getResponse)
      setproductDetails(getResponse)
      setTags(getResponse?.tags ? getResponse?.tags : [])
      getProjectDetails.success = false
    }
  }, [getProjectDetails])

  // handle edit details
  useEffect(() => {
    if (editProject.success === true) {
      modalClose()
      setInitialValue(intial_form_value)
      enqueueSnackbar(
        `${initialValue?.projectName} Project Update Successfully`,
        { variant: 'Success', autoHideDuration: 2000 }
      )
      editProject.success = false
    } else if (editProject.name == 'AxiosError') {
      enqueueSnackbar('An error occurred while editing the project', {
        variant: 'error',
        autoHideDuration: 2000
      })
      editProject.name = ''
    }
  }, [editProject])
  const handleClose = () => {
    setOpen(false)
  }

  const Get_project_Details = async (param) => {
    if (param) {
      disPatch(
        getProjectDetailsRequest({
          projectID: [param?.projectID],
          identifiers: [param?.projectIdentifier]
        })
      )
    }
  }

  const addLine = () => {
    setTags([...Tags, { key: '', value: '' }])
  }

  const handleFieldChange = (index, fieldName, value) => {
    const updatedTags = [...Tags]
    updatedTags[index][fieldName] = value
    setTags(updatedTags)
  }

  const checkTags = (param) => {
    const emptyNameObjects = param.filter((item) => !item.key || !item.value)
    const tag_empty_index = param
      .map((object, index) => {
        if (!object.key || !object.value) {
          return index
        }
        return null
      })
      .filter((index) => index !== null)
    setErrors(tag_empty_index)
    if (emptyNameObjects.length === 0) {
      return true
    }
    setMessage('Tags is empty')
    setSeverity('error')
    setOpen(true)
    setDisableButton(true)
    return false
  }

  const deleteLine = async (line, index) => {
    const checkTagExists =
      productDetails?.tags?.length > 0
        ? productDetails?.tags?.some((param) => param?.key == line?.key)
        : false
    /// if try to delete already added tag in Product details it comes under this conditions
    if (checkTagExists) {
      const check_key_value =
        productDetails?.tags[index]?.key == line?.key &&
        productDetails?.tags[index]?.value == line?.value
      if (check_key_value) {
        // formatting payload
        const setpayload = {
          projectID: editData?.projectID,
          updates: [
            {
              update: 'deletetag',
              tag: { key: line?.key }
            }
          ]
        }
        const response = await manageApicalls(
          '/projects/updateProject',
          setpayload
        )
        // error handling
        if (response.data.success) {
          enqueueSnackbar(`${line?.key} tag Deleted`, {
            variant: 'success',
            autoHideDuration: 2000
          })
          Get_project_Details(editData)
        } else {
          enqueueSnackbar(`Error in Delete ${line?.key} tag`, {
            variant: 'error',
            autoHideDuration: 2000
          })
        }
      } else {
        const updatedTags = [...Tags]
        updatedTags.splice(index, 1)
        setTags(updatedTags)
      }
    } else {
      const updatedTags = [...Tags]
      updatedTags.splice(index, 1)
      setTags(updatedTags)
    }
  }

  const handleSubmit = async (data) => {
    // handleSubmit function is handle the two operation (create & edit)
    setDisableButton(false)
    if (editData) {
      // under if condtion is checking the changes in edited data
      if (
        data.projectName !== productDetails.projectName ||
        data.projectDesc !== productDetails.projectDesc ||
        JSON.stringify(Tags) !== JSON.stringify(productDetails.tags)
      ) {
        const changes = {}
        // update  the changes value into changes object
        if (data.projectName !== productDetails.projectName) {
          changes.projectName = data.projectName
        }
        if (data.projectDesc !== productDetails.projectDesc) {
          changes.projectDesc = data.projectDesc
        }
        if (JSON.stringify(Tags) !== JSON.stringify(productDetails.tags)) {
          changes.tags = Tags
        }
        const objectLength = Object.keys(changes).length

        if (objectLength > 0) {
          const mappedObjects = []
          /// pushing all the chnage value into mappedObject array
          if (changes.projectName) {
            mappedObjects.push({
              update: 'projectName',
              newvalue: changes.projectName
            })
          }
          if (changes.projectDesc) {
            mappedObjects.push({
              update: 'projectDesc',
              newvalue: changes.projectDesc
            })
          }
          if (changes?.tags?.length > 0) {
            changes?.tags?.map((param) => {
              const check_key_exists = productDetails?.tags?.some(
                (val) => val?.key == param?.key && val?.value == param?.value
              )
              if (!check_key_exists) {
                mappedObjects.push({
                  update: 'key',
                  tag: param
                })
              }
            })
          }
          const payload = {
            // formatting payload
            projectID: data?.projectID,
            updates: mappedObjects
          }
          // dispatch edit api call
          if (checkTags(Tags)) {
            /// checkTags function is checking the empty feilds in tag
            disPatch(editprojectRequest(payload))
          }
        } else {
          setDisableButton(true)
          enqueueSnackbar('No Changes', {
            variant: 'info',
            autoHideDuration: 2000
          })
        }
      } else {
        // if there is no changes then this condion will work
        setDisableButton(true)
        enqueueSnackbar('No Changes', {
          variant: 'info',
          autoHideDuration: 2000
        })
      }
    } else {
      // this else condtion works while we create the project
      if (checkTags(Tags)) {
        /// checkTags function is checking the empty feilds in tag
        setDisableButton(false)
        const setpayload =
          Tags?.length > 0
            ? {
                ...data,
                projectIdentifier: data?.projectIdentifier,
                tags: Tags
              }
            : { ...data, projectIdentifier: data?.projectIdentifier, tags: [] }
        disPatch(createProjectRequest(setpayload))
      }
    }
  }

  useEffect(() => {
    Get_project_Details(editData)
  }, [editData])

  const make_disable_input = (param) => {
    if (param >= 0) {
      return productDetails?.tags?.length > param
    }
  }

  return (
    <Formik
      enableReinitialize
      initialValues={initialValue}
      validationSchema={validationSchema}
      innerRef={formikRef}
      onSubmit={(data, { setSubmitting }) => {
        setSubmitting(true)
        handleSubmit(data)
        setSubmitting(false)
      }}
    >
      {({ handleChange, touched, errors, values }) => (
        <Form className="createProject-form">
          <div>
            <Typography textAlign="center" className="createProject-form-title">
              {moduleTitle} Project
            </Typography>
          </div>
          <div className={Tags.length > 0 ? 'scroll' : 'nonscroll'}>
            <div className="createProject-form-outBox">
              <div className="create-Project-form-innerBox">
                <Typography className="createProject-form-pName">
                  Project Name
                  <span className="required-indicator">*</span>
                </Typography>
                <TextField
                  fullWidth
                  value={values.projectName}
                  inputProps={{ maxLength: 255 }}
                  name="projectName"
                  error={touched.projectName && errors.projectName}
                  onChange={handleChange}
                />
                <ErrorMessage
                  name="projectName"
                  component="div"
                  className="Error"
                />
              </div>
              <div>
                <Typography className="createProject-form-pDescription">
                  Project Description
                  <span className="required-indicator">*</span>
                </Typography>
                <TextField
                  multiline
                  fullWidth
                  minRows={4}
                  inputProps={{ maxLength: 1000 }}
                  value={values.projectDesc}
                  maxRows={4}
                  name="projectDesc"
                  onChange={handleChange}
                  error={touched.projectDesc && errors.projectDesc}
                />
                <ErrorMessage
                  name="projectDesc"
                  component="div"
                  className="Error"
                />
              </div>
              <div>
                <Typography className="createProject-form-pIdentifier">
                  Project Identifier
                </Typography>
                {editData
                  ? (
                  <TextField
                    disabled
                    value={values.identifier}
                    inputProps={{ maxLength: 36 }}
                    fullWidth
                    name="identifier"
                    onChange={handleChange}
                  />
                    )
                  : (
                  <TextField
                    fullWidth
                    inputProps={{ maxLength: 36 }}
                    name="projectIdentifier"
                    onChange={handleChange}
                  />
                    )}
              </div>
              {Tags.length > 0 && (
                <div>
                  {Tags.map((line, index) => (
                    <Box mb={2} mt={2} key={index}>
                      <Grid
                        container
                        spacing={22}
                        direction="row"
                        justifyContent="space-around"
                        alignItems="center"
                      >
                        <Grid item xs={4}>
                          <TextField
                            label="Key"
                            className="tag-textField"
                            variant="outlined"
                            // error={Errors?.includes(index) ? true : false}
                            error={
                              !!(
                                Tags[index].key === '' &&
                                Errors?.includes(index)
                              )
                            }
                            value={line.key}
                            // required
                            name={`key${index}`}
                            disabled={make_disable_input(index)}
                            onChange={(e) =>
                              handleFieldChange(index, 'key', e.target.value)
                            }
                          />
                        </Grid>
                        <Grid item xs={4}>
                          <TextField
                            label="Value"
                            className="tag-textField"
                            variant="outlined"
                            // error={Errors.includes(index) ? true : false}
                            error={
                              !!(
                                Tags[index].value === '' &&
                                Errors?.includes(index)
                              )
                            }
                            value={line.value}
                            disabled={make_disable_input(index)}
                            // required
                            name={`value${index}`}
                            onChange={(e) => {
                              handleFieldChange(index, 'value', e.target.value)
                              handleChange(e.target.value)
                            }}
                          />
                        </Grid>
                        <Grid item xs={4}>
                          <Button
                            variant="outlined"
                            color="secondary"
                            onClick={() => deleteLine(line, index)}
                          >
                            <DeleteIcon />
                          </Button>
                        </Grid>
                      </Grid>
                    </Box>
                  ))}
                </div>
              )}
            </div>
          </div>
          <div className="createProject-form-actionButtion">
            <Button
              variant="contained"
              color="primary"
              fullWidth
              disabled={Tags.length === 50}
              onClick={addLine}
              className="createProject-form-actionButtion-tagButton"
            >
              {' '}
              Add Tag
            </Button>

            <div className="createProject-form-actionButtion-controler">
              <Button
                type="submit"
                variant="contained"
                size="large"
                className="createProject-form-actionButtion-controler-submit"
                disabled={!disableButton}
              >
                Submit
              </Button>
              <Button
                variant="contained"
                size="large"
                color="error"
                className="createProject-form-actionButtion-controler-cancel"
                onClick={() => {
                  modalClose()
                  setInitialValue('')
                  //   getProjectDetails = '';
                }}
              >
                Cancel
              </Button>
            </div>
          </div>
        </Form>
      )}
    </Formik>
  )
}

export default CreateProject
