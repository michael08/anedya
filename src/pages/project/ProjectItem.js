import React, { useState } from 'react'
import Stack from '@mui/material/Stack'
import { Card, IconButton, Typography } from '@mui/material'
import Button from '@mui/material/Button'
import MoreHorizIcon from '@mui/icons-material/MoreHoriz'
import styled from '@emotion/styled'
import { Delete, Edit, RemoveRedEye } from '@mui/icons-material'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogContentText from '@mui/material/DialogContentText'
import DialogTitle from '@mui/material/DialogTitle'
import Menu from '@mui/material/Menu'
import MenuItem from '@mui/material/MenuItem'
import TextField from '@mui/material/TextField'
import { useNavigate } from 'react-router-dom'
import StarBorderIcon from '@mui/icons-material/StarBorder'
import StarIcon from '@mui/icons-material/Star'
import useManageApi from '../custom-Hooks/useManageApi'
import { jwtDecode } from 'jwt-decode'
import Bookmark from './BookMark'

const Item = styled('span')(({ theme }) => ({
  padding: '10px'
}))

function ProjectItem (props) {
  const navigate = useNavigate()
  const {
    user, delete_data, Reload, handleOpenEdit
  } = props
  const { manageApicalls } = useManageApi()
  const [anchorEl, setAnchorEl] = React.useState(null)
  const menuopen = Boolean(anchorEl)
  const [open, setOpen] = React.useState({ open: false, data: null })
  const [deleteText, setDeleteText] = React.useState('')
  const [bookmark, setBookMark] = useState(false)
  // Project Delete functon
  const handleDeleteOk = (deleteId) => {
    const authToken = localStorage.getItem('auth-token')
    const decodedToken = jwtDecode(authToken)
    if (deleteId.favorite === true) {
      manageApicalls('/favourite_project/user/', `${decodedToken?.UUID}/${deleteId?.projectID}`)
      delete_data(deleteId)
    } else {
      delete_data(deleteId)
    }
    setOpen({ open: false, data: null })
  }

  const handleClose = () => {
    setDeleteText('')
    setOpen({ open: false, data: null })
  }

  const handlemenuClick = (event) => {
    setAnchorEl(event.currentTarget)
  }
  const handlemenuClose = () => {
    setAnchorEl(null)
  }
  const handleEdit = (project) => {
    handleOpenEdit(project)
    setAnchorEl(null)
  }
  const handleDelete = (project) => {
    setOpen({ open: true, data: project })
    setAnchorEl(null)
  }

  const reDirect = (data) => {
    // getprojectid(data);
    navigate('/dashboards/Project/Projectdetails')
  }
  const handleClick = () => {
    setBookMark(!bookmark)
  }
  return (
    <>
      <Card className="projectItemPage">
        <Stack direction="row" alignItems="center" className="projectItemPage-innerComponent">
          <Item>
            <Stack direction="row" alignItems="center">
              <Item>
              <Bookmark
                  value={props.user.favorite}
                  get_favorite={props.Favorite}
                  reLoade={props}
                  data={props}
                  sx={{ verticalAlign: 'middle' }}
                />
              </Item>
              <Item>
                <Typography
                  variant="h6"
                  mt={1}
                  className="projectItemPage-pName"
                  lineHeight={1.25}
                  onClick={() => reDirect(user)}
                >
                  {user.projectName}
                </Typography>
              </Item>
            </Stack>
          </Item>

          <Item
            sx={{
              alignSelf: 'flex-start',
              flexBasis: { md: '28%', lg: '18%' },
              display: { xs: 'none', md: 'block' }
            }}
          />

          <Item
            sx={{
              flexBasis: '30%',
              display: { xs: 'none', lg: 'block' }
            }}
          >
            <Stack
              spacing={2}
              direction="row"
              alignItems="center"
              sx={{ textAlign: 'center' }}
            />
          </Item>
          <Item
            sx={{
              ml: 'auto',
              display: { xs: 'none', sm: 'block' }
            }}
          />
          <Item sx={{ ml: { xs: 'auto', sm: 0 } }}>
            <IconButton
              aria-label="settings"
              id="basic-button"
              aria-controls={menuopen ? 'basic-menu' : undefined}
              aria-haspopup="true"
              aria-expanded={menuopen ? 'true' : undefined}
              onClick={handlemenuClick}
            >
              <MoreHorizIcon />
            </IconButton>
          </Item>
        </Stack>
      </Card>
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={menuopen}
        onClose={handlemenuClose}
        MenuListProps={{
          'aria-labelledby': 'basic-button'
        }}
      >
        <MenuItem onClick={() => reDirect(user)}>
          <RemoveRedEye className="ProjectItem-Icon" />
          {' '}
          View
        </MenuItem>
        <MenuItem onClick={() => handleEdit(user)}>
          <Edit className="ProjectItem-Icon" />
          {' '}
          Edit
        </MenuItem>
        <MenuItem onClick={() => handleDelete(user)}>
          <Delete className="ProjectItem-Icon" />
          {' '}
          Delete
        </MenuItem>
      </Menu>
      <Dialog
        open={open.open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Are you sure?</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Please confirm the deletion of this project by entering 'DELETE' in
            the field below
            <div sx={{ display: 'flex', justifyContent: 'center', mt: 2 }}>
              <TextField
                id="outlined-basic"
                type="text"
                fullWidth
                value={deleteText}
                onChange={(e) => setDeleteText(e.target.value)}
                variant="outlined"
                autoComplete="off"
              />
            </div>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            type="submit"
            variant="contained"
            size="large"
            className="create_project_submit-but"
            disabled={deleteText?.toLocaleLowerCase() !== 'delete'}
            onClick={() => handleDeleteOk(open.data)}
          >
            Delete
          </Button>
          <Button
            variant="contained"
            size="large"
            color="error"
            className="create_project_submit-but"
            onClick={handleClose}
          >
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
}

export default ProjectItem
