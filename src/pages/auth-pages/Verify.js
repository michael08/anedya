import React, { useState, useEffect } from 'react'
import {
  Card,
  CardContent,
  Typography,
  TextField,
  Button
} from '@mui/material'
import { alpha } from '@mui/material/styles'
import * as Yup from 'yup'
import { Form, Formik, Field, ErrorMessage } from 'formik'
import { useLocation, useNavigate } from 'react-router-dom'
import { useSnackbar } from 'notistack'
import useManageApi from '../custom-Hooks/useManageApi'

const validationSchema = Yup.object().shape({
  code: Yup.string().required('OTP is required')
})

function Verify () {
  const [timer, setTimer] = useState(30) // Set initial timer value
  const [isTimerActive, setIsTimerActive] = useState(false)
  const { enqueueSnackbar } = useSnackbar()
  const { manageApicalls } = useManageApi()

  useEffect(() => {
    let interval
    if (isTimerActive) {
      interval = setInterval(() => {
        setTimer((prevTimer) => prevTimer - 1)
      }, 1000)
    }
    if (timer === 0) {
      setIsTimerActive(false)
    }
    return () => {
      clearInterval(interval)
    }
  }, [timer, isTimerActive])

  const startTimer = () => {
    setIsTimerActive(true)
    setTimer(30)
    setIsTimerActive(true)
  }

  const navigate = useNavigate()
  const location = useLocation()
  const emaildata = location.state

  useEffect(() => {
    startTimer()
    if (!emaildata) {
      navigate('/user/verify')
    }
  }, [])

  const onVerify = async (data) => {
    data.code = Number(data.code)
    try {
      const setpayload = JSON.stringify(data)
      const response = await manageApicalls('/users/verify', setpayload)
      if (response.data.success) {
        // Handle success
        if (!data.resend && response?.data?.data?.verified) {
          enqueueSnackbar('Verify successful', {
            variant: 'success',
            autoHideDuration: 2000
          })
          navigate('/')
        } else {
          enqueueSnackbar('Resent OTP successful', {
            variant: 'success',
            autoHideDuration: 2000
          })
          startTimer()
        }
      }
    } catch (error) {
      enqueueSnackbar(error.response.data.error, {
        variant: 'error',
        autoHideDuration: 2000
      })
    }
  }

  return (
    <div className="otpVerify">
    <Card className="otpVerify-card">
      <CardContent
        className="otpVerify-card-content">
        <div
          className="otpVerify-card-content-imgSection">
          <div className="otpVerify-card-content-imgSection-logo">
            <img
              src="https://cdn.anedya.io/anedya_logo.svg"
              width="200"
              alt="Anedya"
            />
          </div>
        </div>
      </CardContent>
      <CardContent className="otpVerify-card-contentForm">
        <div>
          <Typography className="otpVerify-card-contentForm-header" variant="h5">
            OTP
          </Typography>
        </div>
        <Formik
          initialValues={{
            verifytype: 'email',
            code: '', // Initial value for verifytype field
            Verify: emaildata,
            resend: false
          }}
          onSubmit={(data) => {
            onVerify(data)
          }}
          validationSchema={validationSchema}>
          <Form className="otpVerify-card-contentForm-form" noValidate autoComplete="off">
            <Field
              name="code"
              as={TextField}
              label="Enter OTP"
              fullWidth
              variant="outlined"
              margin="normal"
              inputProps={{ maxLength: 6 }}
              required
            />
            <ErrorMessage name="code" component="div" className="otpVerify-card-contentForm-form-error" />

            <Button
              fullWidth
              type="submit"
              variant="contained"
              size="large"
              className="otpVerify-card-contentForm-form-button">
              Verify
            </Button>

            <div className="otpVerify-card-contentForm-form-resend">
              <Typography className="otpVerify-card-contentForm-form-resend-link" variant="h6">Didn't receive a code ?</Typography>
            </div>

            {isTimerActive
              ? (
              <div className="otpVerify-card-contentForm-form-resend">
                {' '}
                <Typography className="otpVerify-card-contentForm-form-resend-link" >Resend OTP in{timer}s</Typography>
              </div>
                )
              : (
              <div
                className="otpVerify-card-contentForm-form-resendOtp"
                onClick={() =>
                  onVerify({
                    verifytype: 'email',
                    Verify: emaildata,
                    resend: true
                  })
                }>
                {' '}
                <Typography className="otpVerify-card-contentForm-form-resend-link" >
                  Resend OTP
                </Typography>
              </div>
                )}
          </Form>
        </Formik>
      </CardContent>
    </Card>
  </div>
  )
}

export default Verify
