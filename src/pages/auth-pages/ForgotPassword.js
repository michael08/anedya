import React, { useEffect, useRef, useState } from 'react'
import { Card, CardContent, TextField, Typography } from '@mui/material'
import { Link, useNavigate } from 'react-router-dom'
import Button from '@mui/material/Button'
import { alpha } from '@mui/material/styles'
import Alert from '@mui/material/Alert'
import * as yup from 'yup'
import { Form, Formik } from 'formik'
import { useSnackbar } from 'notistack'
import useManageApi from '../custom-Hooks/useManageApi'
const validationSchema = yup.object({
  otp: yup.string('Enter your OTP').required('OTP is required'),
  Password: yup
    .string('Enter your password')
    .required('Password is required')
    .min(8, 'Password must be at least 8 characters long')
    .matches(
      /^(?=.*[A-Z])(?=.*\d)/,
      'Password must contain at least one uppercase letter and one digit'
    )
})

const validationSchemaEmail = yup.object({
  email: yup
    .string('Enter your Email')
    .email('Email is Invalid')
    .required('Email is required')
})

function ForgotPassword () {
  const [passwordlayout, setpasswordlayout] = useState(false)
  const [isTimerActive, setIsTimerActive] = useState(false)
  const [timer, setTimer] = useState(30) // Set initial timer value
  const formikRef = useRef(null)

  const { enqueueSnackbar } = useSnackbar()
  const { manageApicalls } = useManageApi()
  const navigate = useNavigate()
  useEffect(() => {
    let interval

    if (isTimerActive) {
      interval = setInterval(() => {
        setTimer((prevTimer) => prevTimer - 1)
      }, 1000)
    }

    if (timer === 0) {
      setIsTimerActive(false)
    }

    return () => {
      clearInterval(interval)
    }
  }, [timer, isTimerActive])

  const startTimer = () => {
    setIsTimerActive(true)
    setTimer(30) // Reset timer to 30 seconds
    setIsTimerActive(true)
  }
  useEffect(() => {
    startTimer()
  }, [])

  const handleSubmitOtp = async (data) => {
    const getRef = formikRef?.current?.values /// getting gmail from REF
    const { otp, Password } = data
    const setpayload = {
      useremail: getRef?.email,
      resetinfo: {
        otp: Number(otp),
        passwd: await generatePassword(Password)
      }
    }
    manageApicalls('/users/resetPassword', setpayload)
      .then((res) => {
        enqueueSnackbar('Password reset successfully', {
          variant: 'success'
        })
        navigate('/')
      })
      .catch((err) => {
        enqueueSnackbar(err?.response?.data.error || 'An error occurred', {
          variant: 'error'
        })
      })
  }
  const handleOtpRequest = async (data) => {
    const getRef = formikRef?.current?.values
    const setpayload = { useremail: data?.email ?? getRef.email }
    manageApicalls('/users/resetPassword', setpayload)
      .then(() => {
        enqueueSnackbar(
          `Password Reset Code Sent to ${data?.email ?? getRef.email}`,
          {
            variant: 'success'
          }
        )
        setpasswordlayout(true)
        startTimer()
      })
      .catch((err) => {
        enqueueSnackbar(err?.response?.data?.error || 'Password Reset Failed', {
          variant: 'error'
        })
      })
  }
  const generatePassword = (password) => {
    function stringToHex (str) {
      let hex = ''
      for (let i = 0; i < str.length; i++) {
        hex += str.charCodeAt(i).toString(16).padStart(2, '0')
      }
      return hex
    }
    // Example usage:
    const inputString = password
    const hexString = stringToHex(inputString)
    const credientialhex = stringToHex(hexString)
    return credientialhex
  }
  return (
    <div className="forgotPasswordPage">
      <Card className="forgotPasswordPage-card">
        <CardContent className="forgotPasswordPage-card-content">
          <div className="forgotPasswordPage-card-content-imgSection">
            <div className="forgotPasswordPage-card-content-imgSection-logo">
              <img
                src="https://cdn.anedya.io/anedya_logo.svg"
                width="200"
                alt="Anedya"
              />
            </div>
          </div>
        </CardContent>
        <CardContent className="forgotPasswordPage-card-contentForm">
          <div className="forgotPasswordPage-card-contentForm-heading">
            <Typography className="forgotPasswordPage-card-contentForm-heading-name">
              Forgot Password
            </Typography>
            <Typography className="forgotPasswordPage-card-contentForm-info">
              Don't Worry we will help you
            </Typography>
            <Alert
              severity="info"
              className="forgotPasswordPage-card-contentForm-info"
            >
              Please enter your email. You will receive a OTP message to verify
              your details and reset password.
            </Alert>
          </div>
          {passwordlayout
            ? (
            <>
              <Formik
                validateOnChange
                initialValues={{
                  otp: '',
                  Password: ''
                }}
                validationSchema={validationSchema}
                innerRef={formikRef}
                onSubmit={(data, { setSubmitting }) => {
                  setSubmitting(true)
                  handleSubmitOtp(data)
                  setSubmitting(false)
                }}
              >
                {({
                  handleChange,
                  touched,
                  errors,
                  values,
                  setFieldTouched
                }) => (
                  <Form
                    noValidate
                    autoComplete="off"
                    className="forgotPasswordPage-card-contentForm-form"
                  >
                    <div className="forgotPasswordPage-card-contentForm-form-otp">
                      <TextField
                        fullWidth
                        id="otp"
                        label="OTP"
                        name="otp"
                        value={values.otp || ''}
                        inputProps={{ maxLength: 6 }}
                        onChange={(e) => {
                          const { name } = e.target
                          setFieldTouched(name, true)
                          handleChange(e)
                        }}
                        error={touched.otp && errors.otp}
                        helperText={touched.otp && errors.otp ? errors.otp : ''}
                      />
                    </div>
                    <div className="forgotPasswordPage-card-contentForm-form-password">
                      <TextField
                        fullWidth
                        id="password"
                        label="Password"
                        name="Password"
                        value={values.Password}
                        onChange={(e) => {
                          const { name } = e.target
                          handleChange(e)
                          setFieldTouched(name, true)
                        }}
                        error={touched.Password && errors.Password}
                        helperText={
                          touched.Password && errors.Password
                            ? errors.Password
                            : ''
                        }
                      />
                    </div>
                    <Button
                      fullWidth
                      variant="contained"
                      size="large"
                      type="submit"
                      className="forgotPasswordPage-card-contentForm-form-button"
                    >
                      Reset Password
                    </Button>
                    {isTimerActive
                      ? (
                      <div className="forgotPasswordPage-card-contentForm-form-timer">
                        {' '}
                        <span>
                          Resend OTP in
                          {timer}s
                        </span>
                      </div>
                        )
                      : (
                      <Button
                        fullWidth
                        variant="contained"
                        className="forgotPasswordPage-card-contentForm-form-resendOtp"
                        size="large"
                        sx={{ mb: 3 }}
                        onClick={() => {
                          handleOtpRequest()
                        }}
                      >
                        Resend OTP
                      </Button>
                        )}
                  </Form>
                )}
              </Formik>
            </>
              )
            : (
            <>
              <Formik
                validateOnChange
                initialValues={{
                  email: ''
                }}
                validationSchema={validationSchemaEmail}
                innerRef={formikRef}
                onSubmit={(data, { setSubmitting }) => {
                  setSubmitting(true)
                  handleOtpRequest(data)
                  setSubmitting(false)
                }}
              >
                {({
                  handleChange,
                  touched,
                  errors,
                  values,
                  setFieldTouched
                }) => (
                  <>
                    <Form>
                      <div style={{ marginBottom: '30px', marginTop: '10px' }}>
                        <TextField
                          fullWidth
                          id="email"
                          label="Email"
                          name="email"
                          value={values.email}
                          onChange={(e) => {
                            const { name } = e.target
                            setFieldTouched(name, true)
                            handleChange(e)
                          }}
                          error={touched.email && errors.email}
                          helperText={
                            touched.email && errors.email ? errors.email : ''
                          }
                        />
                      </div>
                      <Button
                        fullWidth
                        variant="contained"
                        size="large"
                        sx={{ mb: 3 }}
                        type="submit"
                      >
                        Send OTP
                      </Button>
                    </Form>
                  </>
                )}
              </Formik>
            </>
              )}
          <Typography variant="body1" textAlign="center">
            <Link
              className="forgotPasswordPage_Card_Content_ImgSection-Link"
              to="/"
            >
              Sign In
            </Link>
          </Typography>
        </CardContent>
      </Card>
    </div>
  )
}

export default ForgotPassword
