import React, { useState, useEffect } from 'react'
import {
  Card,
  CardContent,
  Checkbox,
  FormControlLabel,
  TextField,
  IconButton,
  Typography,
  Snackbar,
  CardActions,
  Button
} from '@mui/material'
import { Link, useNavigate } from 'react-router-dom'
import { alpha } from '@mui/material/styles'
import * as yup from 'yup'
import { Form, Formik } from 'formik'
import InputAdornment from '@mui/material/InputAdornment'
import VisibilityIcon from '@mui/icons-material/Visibility'
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff'
import { useSnackbar } from 'notistack'

// import { getAssetPath } from '../../../utils/appHelpers';
// import { ASSET_IMAGES } from '../../../utils/constants/paths';
import useManageApi from '../custom-Hooks/useManageApi'
const validationSchema = yup.object({
  userid: yup
    .string('Enter your email')
    .email('Enter a valid email')
    .required('Email is required'),
  credential: yup
    .string('Enter your password')
    .required('Password is required')
})

function Login () {
  const navigate = useNavigate()
  const userName = localStorage.getItem('username')
  const userPassword = localStorage.getItem('password')

  const { manageApicalls } = useManageApi() /// custom hook  to manage api calls
  const { enqueueSnackbar } = useSnackbar()

  useEffect(() => {
    if (userName) {
      setRememberMeCheck(true)
    }
  }, [])

  const [rememberMeCheck, setRememberMeCheck] = useState(false)
  const [showPassword, setShowPassword] = useState(false)

  const handleTogglePasswordVisibility = () => {
    setShowPassword(!showPassword)
  }
  const handleOnSignIn = (data) => {
    if (rememberMeCheck) {
      localStorage.setItem('username', data.userid)
      localStorage.setItem('password', data.credential)
    } else {
      localStorage.removeItem('username')
      localStorage.removeItem('password')
    }

    function stringToHex (str) {
      let hex = ''
      for (let i = 0; i < str.length; i++) {
        hex += str.charCodeAt(i).toString(16).padStart(2, '0')
      }
      return hex
    }

    const inputString = data.credential
    const hexString = stringToHex(inputString)
    const credientialhex = stringToHex(hexString)
    const password = credientialhex
    const url = '/users/login'
    const payload = { ...data, credential: password }

    manageApicalls(url, payload)
      .then((response) => {
        localStorage.setItem('auth-token', response?.data?.data?.token)
        // setAuthValues({ authToken: response?.data?.data?.token, authUser: data.userid });
        enqueueSnackbar('Login successful', {
          variant: 'success',
          autoHideDuration: 2000
        })
        navigate('/dashboards/Project', { state: data.userid })
      })
      .catch((error) => {
        if (error.response) {
          if (error?.response?.data?.error) {
            enqueueSnackbar(`${error?.response?.data?.error}`, {
              variant: 'error',
              autoHideDuration: 2000
            })
          } else {
            enqueueSnackbar(`${error}`, {
              variant: 'Network error',
              autoHideDuration: 2000
            })
          }
        } else {
          enqueueSnackbar(`${error}`, {
            variant: 'Network error',
            autoHideDuration: 2000
          })
        }
      })
  }

  return (
    <div className="loginPage">
      <Card className="loginPage-card">
        <CardContent className="loginPage-card-content">
          <div className="loginPage-card-content-imgSection">
            <div className="loginPage-card-content-imgSection-logo">
              <img
                src="https://cdn.anedya.io/anedya_logo.svg"
                width="200"
                alt="Anedya"
              />
            </div>
          </div>
        </CardContent>
        <CardContent className="loginPage-card-contentForm">
          <Formik
            validateOnChange
            initialValues={{
              logintype: 'email',
              credential: userPassword || '',
              userid: userName || ''
            }}
            validationSchema={validationSchema}
            onSubmit={(data, { setSubmitting }) => {
              setSubmitting(true)
              handleOnSignIn(data)
              setSubmitting(false)
            }}
          >
            {({
              handleChange,
              touched,
              errors,
              values,
              setFieldTouched,
              handleBlur
            }) => (
              <Form
                className="loginPage-card-contentForm-form"
                noValidate
                autoComplete="off"
              >
                <div>
                  <Typography
                    variant="h5"
                    className="loginPage-card-contentForm-form-heading"
                  >
                    Sign In
                  </Typography>
                </div>
                <div className="loginPage-card-contentForm-form-Emai">
                  <TextField
                    fullWidth
                    name="userid"
                    label="Email"
                    onBlur={handleBlur}
                    onChange={(e) => {
                      const { name } = e.target
                      setFieldTouched(name, true)
                      handleChange(e)
                    }}
                    helperText={
                      touched.userid && errors.userid ? errors.userid : ''
                    }
                    value={values.userid}
                    error={touched.userid && errors.userid}
                    required
                  />
                </div>
                <div className="loginPage-card-contentForm-form-Password">
                  <TextField
                    fullWidth
                    name="credential"
                    type={showPassword ? 'text' : 'password'}
                    label="Password"
                    value={values?.credential}
                    onChange={(e) => {
                      const { name } = e.target
                      setFieldTouched(name, true)
                      handleChange(e)
                    }}
                    helperText={
                      touched.credential && errors.credential
                        ? errors.credential
                        : ''
                    }
                    error={touched.credential && errors.credential}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton onClick={handleTogglePasswordVisibility}>
                            {showPassword
                              ? (
                              <VisibilityIcon />
                                )
                              : (
                              <VisibilityOffIcon />
                                )}
                          </IconButton>
                        </InputAdornment>
                      )
                    }}
                    required
                  />
                </div>
                <div>
                  <FormControlLabel
                    control={
                      <Checkbox
                        name="rememberMe"
                        checked={rememberMeCheck}
                        onChange={(e) => {
                          setRememberMeCheck(e.target.checked)
                        }}
                      />
                    }
                    label="Remember me"
                  />
                </div>
                <CardActions className="loginPage-card-contentForm-form-cardAction">
                  <Button
                    variant="contained"
                    type="submit"
                    className="loginPage-card-contentForm-form-cardAction-button"
                    fullWidth
                    sx={{ fontSize: '14px', p: 4, height: 40 }}
                  >
                    SIGN IN
                  </Button>
                </CardActions>
                <Typography textAlign='center' mb={2} mt={3} className="loginPage-card-contentForm-form-forgetPassword">
                  Forgot your password?
                  <Link
                    className="Link"
                    to="/user/forgetpassword"
                    color="inherit"
                    underline="none"
                  >
                    {' '}
                    Recover Now
                  </Link>
                </Typography>
                <Typography className="loginPage-card-contentForm-form-link" textAlign='center'>
                  Don't have an account?
                  <Link className="Link" to="/user/SignUp">
                    Register Now
                  </Link>
                </Typography>
              </Form>
            )}
          </Formik>
        </CardContent>
      </Card>
    </div>
  )
}

export default Login
