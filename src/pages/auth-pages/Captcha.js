import ReCAPTCHA from 'react-google-recaptcha'

function Captcha (props) {
  const recaptcha_onChane = (value) => {
    if (value.length !== 0) {
      props.isvalid(true)
      if (value.length === 0) {
        props.isError(true)
      } else {
        props.isError(false)
      }
    }
  }

  return (
    <ReCAPTCHA
      sitekey="6LcgSQcoAAAAAFa7SeeNpMJa-E8Kawtx3gIFXKif"
      onChange={recaptcha_onChane}
    />
  )
}

export default Captcha
