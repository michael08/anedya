import React, { useState } from 'react'
import {
  Card,
  CardContent,
  TextField,
  Typography,
  Snackbar
} from '@mui/material'
import { Link, useNavigate } from 'react-router-dom'
import Button from '@mui/material/Button'
import * as yup from 'yup'
import { Form, Formik } from 'formik'

import { useSnackbar } from 'notistack'
import IconButton from '@mui/material/IconButton'
import InputAdornment from '@mui/material/InputAdornment'
import VisibilityIcon from '@mui/icons-material/Visibility'
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff'

import useManageApi from '../custom-Hooks/useManageApi'
import Captcha from './Captcha'

const validationSchema = yup.object({
  email: yup
    .string('Enter your email')
    .email('Enter a valid email')
    .required('Email is required'),
  credential: yup
    .string('Enter your password')
    .required('Password is required')
    .min(8, 'Password must be at least 8 characters long')
    .matches(
      /^(?=.*[A-Z])(?=.*\d)/,
      'Password must contain at least one uppercase letter and one digit'
    ),
  last_name: yup
    .string('Enter your Last Name')
    .required('Last Name is required'),
  first_name: yup
    .string('Enter your First Name')
    .required('First Name is required'),
  confirmPassword: yup
    .string('Confirm your password')
    .oneOf([yup.ref('credential')], 'Passwords must match') // Compares to the 'credential' field
    .required('Confirm Password is required')
})

function Signup () {
  const { manageApicalls } = useManageApi() // custom hook for manage API
  const { enqueueSnackbar } = useSnackbar()
  const [buttonDisable, setButtonDisable] = useState(true)
  const [showPassword, setShowPassword] = useState(false)
  const [showConformPassword, setShowConformPassword] = useState(false)

  const handleTogglePasswordVisibility = () => {
    setShowPassword(!showPassword) /// password visibility function
  }
  /// confirm  password visibility function
  const handleToggleConformPasswordVisibility = () => {
    setShowConformPassword(!showConformPassword)
  }

  const navigate = useNavigate()
  // captch verify function
  const handleCaptch = (param) => {
    if (param) {
      setButtonDisable(false)
    }
  }
  /// form submit
  const handleSubmit = async (data) => {
    try {
      function stringToHex (str) {
        let hex = ''
        for (let i = 0; i < str.length; i++) {
          hex += str.charCodeAt(i).toString(16).padStart(2, '0')
        }
        return hex
      }
      const inputString = data.credential
      const hexString = stringToHex(inputString)
      const credientialhex = stringToHex(hexString)
      const password = credientialhex
      const setpayload = JSON.stringify({ ...data, credential: password })
      const response = await manageApicalls('/users/signup', setpayload)
      if (response.data.success) {
        localStorage.removeItem('auth-token')
        enqueueSnackbar('Signup successful', {
          variant: 'success',
          autoHideDuration: 2000
        })
        navigate('/user/verify', { state: data.email })
      }
    } catch (error) {
      enqueueSnackbar(`${error.response.data.error}`, {
        variant: 'error',
        autoHideDuration: 2000
      })
    }
  }

  return (
    <div className="signupForm">
      <Card className="signupForm-card">
        <CardContent className="signupForm-card-content">
          <div className="signupForm-card-content-imgSection">
            <div className="signupForm-card-content-imgSection-logo">
              <img
                src="https://cdn.anedya.io/anedya_logo.svg"
                width="200"
                alt="ANEDYA"
              />
            </div>
          </div>
        </CardContent>
        <CardContent className="signupForm-card-contentForm">
        <Formik
            validateOnBlur
            validateOnChange
            initialValues={{
              email: '',
              mobile: '7487654964',
              first_name: '',
              last_name: '',
              credential: '',
              confirmPassword: ''
            }}
            validationSchema={validationSchema}
            onSubmit={(data, { setSubmitting }) => {
              setSubmitting(true)
              handleSubmit(data)
              setSubmitting(false)
            }}>
            {({
              isSubmitting,
              touched,
              errors,
              handleChange,
              handleBlur,
              setFieldTouched,
              values
            }) => (
                <Form
                  className="signupForm-card-contentForm-form"
                  noValidate
                  autoComplete="off">
                  <div className="signupForm-card-contentForm-form-heading">
                    <Typography variant="h5" className="signupForm-card-contentForm-form-heading-name">
                      Sign Up
                    </Typography>
                  </div>
                  <div className="signupForm-card-contentForm-form-firstNameBox">
                    <TextField
                      fullWidth
                      name="first_name"
                      label="First Name"
                      required
                      onChange={(e) => {
                        const { name } = e.target
                        setFieldTouched(name, true)
                        handleChange(e)
                      }}
                      error={touched.first_name && errors.first_name}
                      helperText={
                        touched.first_name && errors.first_name
                          ? errors.first_name
                          : ''
                      }
                      onBlur={handleBlur}
                    />
                  </div>
                  <div className="signupForm-card-contentForm-form-lastNameBox">
                    <TextField
                      fullWidth
                      name="last_name"
                      label="Last Name"
                      onChange={(e) => {
                        const { name } = e.target
                        setFieldTouched(name, true)
                        handleChange(e)
                      }}
                      required
                      error={touched.last_name && errors.last_name}
                      helperText={
                        touched.last_name && errors.last_name
                          ? errors.last_name
                          : ''
                      }
                      onBlur={handleBlur}
                    />
                  </div>
                  <div className="signupForm-card-contentForm-form-emailNameBox">
                    <TextField
                      fullWidth
                      name="email"
                      label="Email"
                      required
                      onChange={(e) => {
                        const { name } = e.target
                        setFieldTouched(name, true)
                        handleChange(e)
                      }}
                      value={values?.email}
                      error={touched.email && errors.email}
                      helperText={
                        touched.email && errors.email ? errors.email : ''
                      }
                      onBlur={handleBlur}
                    />
                  </div>
                  <div className="signupForm-card-contentForm-form-passwordBox">
                    <TextField
                      fullWidth
                      name="credential"
                      label="Password"
                      type={showPassword ? 'text' : 'password'}
                      required
                      onBlur={handleBlur}
                      onChange={(e) => {
                        const { name } = e.target
                        setFieldTouched(name, true)
                        handleChange(e)
                      }}
                      helperText={
                        touched.credential && errors.credential
                          ? errors.credential
                          : ''
                      }
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            <IconButton
                              onClick={handleTogglePasswordVisibility}>
                              {showPassword
                                ? (
                                <VisibilityIcon />
                                  )
                                : (
                                <VisibilityOffIcon />
                                  )}
                            </IconButton>
                          </InputAdornment>
                        )
                      }}
                      error={touched.credential && errors.credential}
                    />
                  </div>

                  <div className="signupForm-card-contentForm-form-passwordBox">
                    <TextField
                      fullWidth
                      name="confirmPassword"
                      label="ConfirmPassword"
                      type={showConformPassword ? 'text' : 'password'}
                      required
                      onChange={(e) => {
                        const { name } = e.target
                        setFieldTouched(name, true)
                        handleChange(e)
                      }}
                      onBlur={handleBlur}
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            <IconButton
                              onClick={handleToggleConformPasswordVisibility}>
                              {showConformPassword
                                ? (
                                <VisibilityIcon />
                                  )
                                : (
                                <VisibilityOffIcon />
                                  )}
                            </IconButton>
                          </InputAdornment>
                        )
                      }}
                      error={touched.confirmPassword && errors.confirmPassword}
                      helperText={
                        touched.confirmPassword && errors.confirmPassword
                          ? errors.confirmPassword
                          : ''
                      }
                    />
                  </div>
                  <div className="signupForm-card-contentForm-form-captchaBox">
                    <Captcha isvalid={(param) => handleCaptch(param)} />
                  </div>
                  <Typography variant="body1" mb={2} className="signupForm-card-contentForm-form-policy">
                    By signing up, you agree to our{' '}
                    <a
                      href="https://anedya.io/privacy-policy/"
                      target="_blank"
                      rel="noreferrer"
                      className="Link">
                      {' '}
                      Privacy Policy{' '}
                    </a>
                    and{' '}
                    <a
                      href="https://anedya.io/terms-of-services/"
                      target="_blank"
                      rel="noreferrer"
                      className="Link">
                      Terms & Conditions
                    </a>
                  </Typography>

                  <Button
                    fullWidth
                    type="submit"
                    variant="contained"
                    size="large"
                    className="signupForm-card-contentForm-form-button"
                    loading={isSubmitting}
                    disabled={buttonDisable}>
                    SIGNUP
                  </Button>
                  <div className="signupForm-card-contentForm-form-singinLink">
                    <Typography className="signupForm-card-contentForm-form-singinLink-link">
                      Already have an Account?
                      <Link to="/" className="Link">
                        {' '}
                        SignIn
                      </Link>
                    </Typography>
                  </div>
                </Form>
            )
            }
          </Formik>
        </CardContent>
      </Card>
    </div>
  )
}

export default Signup
