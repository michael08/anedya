import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import projectReducer from './projectReducer'

const exportReducers = history => {
  return combineReducers({
    router: connectRouter(history),
    project: projectReducer
  })
}

export default exportReducers
