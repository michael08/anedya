import { projectActionType } from '../actions/projectAction'

const INITIAL_STATE = {
  favLisit: [],
  getProduct: [],
  deleteResponse: [],
  createProject: [],
  editProject: [],
  getProjectDetails: []
}

export default function Project (state = INITIAL_STATE, action) {
  switch (action.type) {
    case projectActionType.GET_FAV_LISIT_SUCCESS: {
      return {
        ...state,
        favLisit: action.payload.items
      }
    }
    case projectActionType.GET_PRODUCT_SUCCESS: {
      return {
        ...state,
        getProduct: action.payload.items
      }
    }
    case projectActionType.DELETE_PRODUCT_SUCCESS: {
      return {
        ...state,
        deleteResponse: action.payload.items
      }
    }
    case projectActionType.CREATE_PRODUCT_SUCCESS: {
      return {
        ...state,
        createProject: action.payload.items
      }
    }
    case projectActionType.EDIT_PROJECT_SUCCESS: {
      return {
        ...state,
        editProject: action.payload.items
      }
    }
    case projectActionType.GET_PROJECT_DETAILS_SUCCESS: {
      return {
        ...state,
        getProjectDetails: action.payload.items
      }
    }

    default: {
      return state
    }
  }
}
