export const projectActionType = {
  GET_FAV_LISIT_REQUEST: 'GET_FAV_LISIT_REQUEST',
  GET_FAV_LISIT_SUCCESS: 'GET_FAV_LISIT_SUCCESS',
  GET_PRODUCT_REQUEST: 'GET_PRODUCT_REQUEST',
  GET_PRODUCT_SUCCESS: 'GET_PRODUCT_SUCCESS',
  DELETE_PRODUCT_REQUEST: 'DELETE_PRODUCT_REQUEST',
  DELETE_PRODUCT_SUCCESS: 'DELETE_PRODUCT_SUCCESS',
  CREATE_PRODUCT_REQUEST: 'CREATE_PRODUCT_REQUEST',
  CREATE_PRODUCT_SUCCESS: 'CREATE_PRODUCT_SUCCESS',
  EDIT_PROJECT_REQUEST: 'EDIT_PROJECT_REQUEST',
  EDIT_PROJECT_SUCCESS: ' EDIT_PROJECT_SUCCESS',
  GET_PROJECT_DETAILS_REQUEST: 'GET_PROJECT_DETAILS_REQUEST',
  GET_PROJECT_DETAILS_SUCCESS: 'GET_PROJECT_DETAILS_SUCCESS'
}
// get fav
export const getFavLisitRequest = (payload) => ({
  type: projectActionType.GET_FAV_LISIT_REQUEST,
  payload
})

export const getFavLisitSuccess = (payload) => ({
  type: projectActionType.GET_FAV_LISIT_SUCCESS,
  payload
})

/// get product list
export const getProductListRequest = (payload) => ({
  type: projectActionType.GET_PRODUCT_REQUEST,
  payload
})

export const getProductListSuccess = (payload) => ({
  type: projectActionType.GET_PRODUCT_SUCCESS,
  payload
})

// delete project
export const deleteProjectData = (payload) => ({
  type: projectActionType.DELETE_PRODUCT_REQUEST,
  payload
})
export const deleteProjectSuccess = (payload) => ({
  type: projectActionType.DELETE_PRODUCT_SUCCESS,
  payload
})
/// create Product
export const createProjectRequest = (payload) => ({
  type: projectActionType.CREATE_PRODUCT_REQUEST,
  payload
})
export const createProjectSuccess = (payload) => ({
  type: projectActionType.CREATE_PRODUCT_SUCCESS,
  payload
})
/// edit  Project
export const editprojectRequest = (payload) => ({
  type: projectActionType.EDIT_PROJECT_REQUEST,
  payload
})
export const editProjectSuccess = (payload) => ({
  type: projectActionType.EDIT_PROJECT_SUCCESS,
  payload
})
/// get  Project details
export const getProjectDetailsRequest = (payload) => ({
  type: projectActionType.GET_PROJECT_DETAILS_REQUEST,
  payload
})
export const getProjectDetailsSuccess = (payload) => ({
  type: projectActionType.GET_PROJECT_DETAILS_SUCCESS,
  payload
})
