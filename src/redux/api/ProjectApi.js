import { BaseURL } from '../../App'
import { jwtDecode } from 'jwt-decode'

const getConfig = () => {
  const token = localStorage.getItem('auth-token')
  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: token
    }
  }
  return config
}
export const GetFavList = async (Id) => {
  const token = localStorage.getItem('auth-token')
  const decodedToken = jwtDecode(token)
  return BaseURL.get(`/favourite_project/user/${decodedToken.UUID}`)
}

/// product list
export const GetProductList = async (data) => {
  const getconfig = await getConfig()
  return BaseURL.post('/projects/listProjects', data, getconfig)
}

/// delete product
export const deleteProduct = async (id) => {
  const getconfig = await getConfig()
  return BaseURL.post('/projects/deleteProject', id, getconfig)
}

// create product
export const createNewProduct = async (payload) => {
  const getconfig = await getConfig()
  return BaseURL.post('/projects/createProject', payload, getconfig)
}
// edit project
export const editNewProject = async (payload) => {
  const getconfig = await getConfig()
  return BaseURL.post('/projects/updateProject', payload, getconfig)
}

// get project details
export const getProjectDetails = async (payload) => {
  const getconfig = await getConfig()
  return BaseURL.post('/projects/details', payload, getconfig)
}
