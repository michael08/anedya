import { applyMiddleware, compose, createStore } from 'redux'
import { createBrowserHistory } from 'history'
import { routerMiddleware } from 'connected-react-router'
import reducers from '../reducers'
import createSagaMiddleware from 'redux-saga'
import rootSaga from '../sagas'

const history = createBrowserHistory()
const sagaMiddleware = createSagaMiddleware()
const routeMiddleware = routerMiddleware(history)
const bindMiddleware = middleware => {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

  return composeEnhancers(applyMiddleware(...middleware))
}

function configureStore (initialState = {}) {
  const store = createStore(reducers(history), initialState, bindMiddleware([routeMiddleware, sagaMiddleware]))
  sagaMiddleware.run(rootSaga)

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers/index', () => {
      const exportReducers = require('../reducers')
      store.replaceReducer(exportReducers)
    })
  }
  return store
}
export default configureStore
export { history }
