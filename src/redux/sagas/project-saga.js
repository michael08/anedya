import { takeEvery, call, put, fork } from 'redux-saga/effects'
import * as project from '../actions/projectAction'
import { GetFavList, GetProductList, deleteProduct, createNewProduct, editNewProject, getProjectDetails } from '../api/ProjectApi'

// get fav saga
function * getFavList ({ payload }) {
  try {
    const result = yield call(GetFavList, payload)
    yield put(project.getFavLisitSuccess({
      items: result.data
    }))
  } catch (e) {
    // console.log(e, "error")
  }
}

function * watchProjectRequest () {
  yield takeEvery(project.projectActionType.GET_FAV_LISIT_REQUEST, getFavList)
}

/// get product saga
function * getProductAll ({ payload }) {
  try {
    const result = yield call(GetProductList, payload)
    yield put(project.getProductListSuccess({
      items: result.data
    }))
  } catch (e) {
    // console.log(e, "error")
  }
}

function * watchgetProductList () {
  yield takeEvery(project.projectActionType.GET_PRODUCT_REQUEST, getProductAll)
}

// delete product
function * deleteProductList ({ payload }) {
  try {
    const result = yield call(deleteProduct, payload)
    yield put(project.deleteProjectSuccess({
      items: result.data

    }))
    if (result?.data?.success === true) {
      yield call(getProductAll)
      yield call(getFavList)
    }
  } catch (e) {
    // console.log(e, "error")
  }
}
function * watchDeleteProduct () {
  yield takeEvery(project.projectActionType.DELETE_PRODUCT_REQUEST, deleteProductList)
}
// create products
function * createProduct ({ payload }) {
  try {
    const result = yield call(createNewProduct, payload)
    yield put(project.createProjectSuccess({
      items: result.data
    }))
    if (result?.data?.success === true) {
      yield call(getProductAll)
    }
  } catch (e) {
    const getError = { error: e.response.data.error, response: false }
    yield put(project.createProjectSuccess({
      items: getError
    }))
  }
}

function * watchCreateProduct () {
  yield takeEvery(project.projectActionType.CREATE_PRODUCT_REQUEST, createProduct)
}

// Edit PROJECT
function * editProject ({ payload }) {
  try {
    const result = yield call(editNewProject, payload)
    yield put(project.editProjectSuccess({
      items: result.data
    }))
  } catch (e) {
    // console.log(e, "error")
  }
}
function * watchEditProject () {
  yield takeEvery(project.projectActionType.EDIT_PROJECT_REQUEST, editProject)
}
// GET PROJECT DETAILS
function * getProjectDetailse ({ payload }) {
  try {
    const result = yield call(getProjectDetails, payload)
    yield put(project.getProjectDetailsSuccess({
      items: result.data
    }))
  } catch (e) {
    // console.log(e, "error")
  }
}
function * watchProjectDetails () {
  yield takeEvery(project.projectActionType.GET_PROJECT_DETAILS_REQUEST, getProjectDetailse)
}
const projectSaga = [
  fork(watchProjectRequest),
  fork(watchgetProductList),
  fork(watchDeleteProduct),
  fork(watchCreateProduct),
  fork(watchEditProject),
  fork(watchProjectDetails)
]
export default projectSaga
