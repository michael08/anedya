import { all } from 'redux-saga/effects'
import ProjectSaga from './project-saga'
export default function * rootSaga () {
  yield all([
    ...ProjectSaga
  ])
}
