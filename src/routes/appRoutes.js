import AdminPanelSettingsIcon from "@mui/icons-material/AdminPanelSettings";
import ContentPasteIcon from "@mui/icons-material/ContentPaste";
import Project from "../Project";
import DataUsageIcon from "@mui/icons-material/DataUsage";
import HelpIcon from "@mui/icons-material/Help";
import Help from "../Pages/Help";
import Usage from "../Pages/Usage";
import Setting from "../Pages/Setting";

const appRoutes = [
  {
    path: "/dashboards/Project",
    element: <Project />,
    state: "Project",
    sidebarProps: {
      displayText: "Project",
      icon: <ContentPasteIcon />,
    },
  },
  {
    path: "/Project/Setting",
    element: <Setting />,
    state: "dashboard",
    sidebarProps: {
      displayText: "Admin",
      icon: <AdminPanelSettingsIcon />,
    },
    child: [
      {
        path: "/Project/Setting",
        element: <Setting/>,
        state: "Setting",
        sidebarProps: {
          displayText: "Setting",
        },
      },
    ],
  },
  {
    path: "/Project/Usage",
    element: <Usage/>,
    state: "Usage",
    sidebarProps: {
      displayText: "Usage",
      icon: <DataUsageIcon />,
    },
  },
  {
    path: "/Project/Help",
    element: <Help/>,
    state: "Help",
    sidebarProps: {
      displayText: "Help",
      icon: <HelpIcon/>,
    },
  },
];

export default appRoutes;
