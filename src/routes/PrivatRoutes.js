import React from 'react'
import SideMenu from './SideMenu'

const PrivateRoutes = ({ children }) => {
  return <div><SideMenu/>{children}</div>
}

export default PrivateRoutes
