import React, { useEffect, useState } from 'react';
import { Collapse, List, ListItemButton, ListItemIcon, ListItemText, Typography } from "@mui/material";
import ExpandLessOutlinedIcon from '@mui/icons-material/ExpandLessOutlined';
import ExpandMoreOutlinedIcon from '@mui/icons-material/ExpandMoreOutlined';
import SidebarItem from "./SidebarItem";
import { alpha } from '@mui/material/styles';
import { useLocation } from 'react-router-dom';

function SidebarItemCollapse({ item }) {
  const [open, setOpen] = useState(false);

  const location = useLocation();
  const pathName = location.pathname;
  useEffect(() => {
    // if (appState.includes(item.state)) {
    //   setOpen(true);
    // }
  }, [item]);

  return (
    item.sidebarProps ? (
      <>
        <ListItemButton
          onClick={() => setOpen(!open)}
          sx={{
            "&:hover": {
                backgroundColor: alpha('#E9ECEF', 0.15),
                color :'#1FA2FF'
              },
            paddingY: "12px",
            paddingX: "24px"
          }}
        >
          <ListItemIcon sx={{
            // "&:hover": {
            //     backgroundColor: alpha('#E9ECEF', 0.15),
            //     color :'#1FA2FF'
            //   },
          }}>
            {item.sidebarProps.icon && item.sidebarProps.icon}
          </ListItemIcon>
          <ListItemText
            disableTypography
            primary={
              <Typography>
                {item.sidebarProps.displayText}
              </Typography>
            }
          />
          {open ? <ExpandLessOutlinedIcon /> : <ExpandMoreOutlinedIcon />}
        </ListItemButton>
        <Collapse in={open} timeout="auto">
          <List>
            {item.child?.map((route, index) => (
              route.sidebarProps ? (
                route.child ? (
                  <SidebarItemCollapse item={route} key={index} />
                ) : (
                  <SidebarItem item={route} key={index} />
                )
              ) : null
            ))}
          </List>
        </Collapse>
      </>
    ) : null
  );
}

export default SidebarItemCollapse;
