import React from 'react'
import { Avatar, Drawer, List, Stack, Toolbar } from '@mui/material'
import appRoutes from './appRoutes'
import SidebarItem from './SidebarItem'
import SidebarItemCollapse from './SidebarItemcollabse'
import Logo from '../Logo/Logo'

const Sidebar = () => {
  return (
    <Drawer
      variant="permanent"
      sx={{
        width: '200px',
        flexShrink: 0,
        '& .MuiDrawer-paper': {
          width: '250px',
          boxSizing: 'border-box',
          borderRight: '0px'
          // backgroundColor: '#F5F7FA',
          // color: colorConfigs.sidebar.color
        }
      }}
    >
      <List disablePadding>
        <Toolbar sx={{ marginBottom: '20px' }}>
          <Stack
            sx={{ width: '100%' }}
            direction="row"
            justifyContent="center"
          >
            <Logo />
          </Stack>
        </Toolbar>
        <div>

        </div>
        {appRoutes.map((route, index) => (
          route.sidebarProps
            ? (
                route.child
                  ? (
              <SidebarItemCollapse item={route} key={index} />
                    )
                  : (
              <SidebarItem item={route} key={index} />
                    )
              )
            : null
        ))}
      </List>
    </Drawer>
  )
}

export default Sidebar
