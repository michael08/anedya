import React, { useState } from 'react'
import { ListItemButton, ListItemIcon } from '@mui/material'
import { Link, useLocation } from 'react-router-dom'
import { alpha } from '@mui/material/styles'

function SidebarItem ({ item }) {
  const [getPath, setGetpath] = useState()
  const location = useLocation()
  const pathName = location.pathname
  return (
    item.sidebarProps && item.path ? (
      <ListItemButton
        component={Link}
        to={item.path}
        onClick={() => { setGetpath(item.path) }}
        sx={{
          '&:hover': {
            backgroundColor: alpha('#E9ECEF', 0.15),
            color: '#1FA2FF'
          },
          backgroundColor: getPath === pathName ? 'rgba(31, 162, 255, 0.15)' : 'unset',
          color: getPath === pathName ? '#1FA2FF' : '',
          paddingY: '12px',
          paddingX: '24px'
        }}
      >
        <ListItemIcon sx={{
          // color: colorConfigs.sidebar.color
          color: getPath === pathName ? '#1FA2FF' : '',
          '&:hover': {
            backgroundColor: '',
            color: '#1FA2FF'
          }
        }}>
          {item.sidebarProps.icon && item.sidebarProps.icon}
        </ListItemIcon>
        {item.sidebarProps.displayText}
      </ListItemButton>
    ) : null
  )
}

export default SidebarItem
