import * as React from "react";
import PropTypes from "prop-types";
import Box from "@mui/material/Box";
import CssBaseline from "@mui/material/CssBaseline";
import Divider from "@mui/material/Divider";
import Drawer from "@mui/material/Drawer";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import { useLocation, Link } from "react-router-dom";
import ContentPasteIcon from "@mui/icons-material/ContentPaste";
import AdminPanelSettingsIcon from "@mui/icons-material/AdminPanelSettings";
import HelpIcon from "@mui/icons-material/Help";
import DataUsageIcon from "@mui/icons-material/DataUsage";
const drawerWidth = 240;

function ResponsiveDrawer(props) {
  const { window } = props;
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const location = useLocation();
  const pathName = location.pathname;
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  const menu = [
    { tittle: "project", path: "/", Icone: <ContentPasteIcon /> },
    { tittle: "Admin", path: "/project", Icone: <AdminPanelSettingsIcon /> },
    { tittle: "Usage", path: "/project", Icone: <DataUsageIcon /> },
    { tittle: "Help", path: "/project", Icone: <HelpIcon /> },
  ];
  const drawer = (
    <div>
      <List>
        {menu.map((item, index) => {
          const { Icone, path, tittle } = item;
          return (
            <ListItem
              key={tittle}
              disablePadding
              component={Link}
              to={item.path}
              button
              path={path}
              selected={item.path === pathName}
            >
              <ListItemButton>
                <ListItemIcon>{Icone}</ListItemIcon>
                <ListItemText primary={tittle} />
              </ListItemButton>
            </ListItem>
          );
        })}
      </List>
      <Divider />
    </div>
  );

  const container =
    window !== undefined ? () => window().document.body : undefined;

  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />

      <Box
        component="nav"
        sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 } }}
        aria-label="mailbox folders"
      >
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Drawer
          container={container}
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          sx={{
            display: { xs: "block", sm: "none" },
            "& .MuiDrawer-paper": {
              boxSizing: "border-box",
              width: drawerWidth,
            },
          }}
        >
          {drawer}
        </Drawer>
        <Drawer
          variant="permanent"
          sx={{
            display: { xs: "none", sm: "block" },
            "& .MuiDrawer-paper": {
              boxSizing: "border-box",
              width: drawerWidth,
            },
          }}
          open
        >
          {drawer}
        </Drawer>
      </Box>
    </Box>
  );
}

ResponsiveDrawer.propTypes = {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

export default ResponsiveDrawer;
