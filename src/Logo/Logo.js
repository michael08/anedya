import React from 'react';
import { Link }from "react-router-dom";

const Logo = ({mini, mode, sx}) => {
    return (
        <div style={{display: "inline-flex",}}>
            <Link to={'/dashboards/project'}>
                {
                    !mini ?
                        <img src={`https://cdn.anedya.io/anedya_logo.svg`} width="150" alt="Jumbo React" style={{ filter: 'invert(20%)'}} />
                        :
                        <img src={`https://cdn.anedya.io/anedya_logo.svg`} width="150" alt="Jumbo React" style={{ filter: 'invert(20%)' }} />
                }
            </Link>
        </div>
    );
};

Logo.defaultProps = {
    mode: "light"
};

export default Logo;
