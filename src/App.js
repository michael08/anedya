import { HashRouter, Route, Swtch, Routes } from 'react-router-dom'
import Login from './pages/auth-pages/Login'
import axios from 'axios'
import Signup from './pages/auth-pages/Signup'
import Verify from './pages/auth-pages/Verify'
import ForgotPassword from './pages/auth-pages/ForgotPassword'
import AppSnackbar from './AppSnackbar'
import '../src/assets/scss/main.scss'
import configureStore from './redux/store'
import { Provider } from 'react-redux'
import Project from './pages/project/Project'
export const BaseURL = axios.create({
  baseURL: 'http://stage1.anedya.io'
})

const store = configureStore()
function App () {
  return (
    <div className="App">
      <Provider store={store}>
        <HashRouter>
          <AppSnackbar>
            <Routes>
              <Route path="/" element={<Login />} />
              <Route path="/user/SignUp" element={<Signup />} />
              <Route path="/user/verify" element={<Verify />} />
              <Route path="/user/verify" element={<Verify />} />
              <Route path="/user/forgetpassword" element={<ForgotPassword />} />
              <Route path="/dashboards/Project" element={<Project/>}/>
            </Routes>
          </AppSnackbar>
        </HashRouter>
      </Provider>
    </div>
  )
}

export default App
